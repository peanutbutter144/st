# TOM IENT'S ST CONFIG

[Forked from here.](https://git.suckless.org/st)

## Tweaks:

 - use `Terminus:pixelsize=16` instead of `Liberation Mono:pixelsize=12`

## Patches:

 - solarized-both color scheme (switch with `F6`)
 - scrollback with `shift+mousewheel` or `shift+pageup/down`
 - hide cursor when typing

## st - simple terminal
st is a simple terminal emulator for X which sucks less.


## Requirements
In order to build st you need the Xlib header files.


## Installation
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

```
make clean install
```

## Running st
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

```
tic -sx st.info
```

See the man page for additional details.

## Credits
Based on Aurélien APTEL \<aurelien dot aptel at gmail dot com\> bt source code.

